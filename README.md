# Infra K8s
This Repository contains Terraform Code to provision a EKS cluster through pipeline to deploy the application configured under https://gitlab.com/karthikdevopscloud1984/pod_lister

## Summary 

This Code pipeline has validate, plan, apply(manual) and destroy(manual). This would create a separate VPC and create EKS cluster in the private subnet along with worker groups for Autoscaling group. A NAT gateway is created for worker node in private to reach internet.It stores the terraform state in the backend using S3 bucket configured. 

Apply and Destroy stage in the pipeline needs to be manually triggered after checking what resources needs to be created or if we want to destroy the cluster.
