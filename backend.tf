terraform {
	backend "s3" {
		bucket = "tf-devops-84"
		key    = "foo/terraform.tfstate"
		region = "us-east-2"
	}
}
